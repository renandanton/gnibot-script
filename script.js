'use strict';


const puppeteer = require('puppeteer-extra');
const StealthPlugin = require('puppeteer-extra-plugin-stealth');
const axios = require('axios');
const chrono = require('chrono-node')
const viewportList = require('viewport-list');
const viewPortSizes = viewportList(['Samsung Galaxy']);
const viewPortSize = viewPortSizes[Math.floor(Math.random()*viewPortSizes.length)];


const proxies = [
  {
    proxyUrl: "http://proxy.fidobox.us:2063",
    key: "gnib_session_turbo_01",
  },
  {
    proxyUrl: "http://proxy.fidobox.us:2067",
    key: "gnib_session_turbo_02"
  },
  {
    proxyUrl: "http://proxy.fidobox.us:2068",
    key: "gnib_session_turbo_03",
  }, 
  {
    proxyUrl: "http://proxy.fidobox.us:2069",
    key: "gnib_session_turbo_04"
  },
  { 
    proxyUrl: "http://proxy.fidobox.us:2070",
    key: "gnib_session_turbo_05"
  }
];

const UserAgent = require('user-agents');
const userAgent = new UserAgent({deviceCategory: 'mobile'});

const website_uri = 'https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppSelect?OpenForm';
const detector = 'https://antcpt.com/score_detector/'
const referer  = "https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppSelect?OpenForm";
const USER_AGENT = userAgent.toString();
const proxySettings =  proxies[Math.floor(Math.random()*proxies.length)]; //proxies[3]; 

const COOKIE_CONSENT = "#cookiescript_accept";
const CATEGORY_SELECTOR = "#Category";
const SUBCATEGORY_SELECTOR = "#SubCategory";
const CONFIRMGNIB_SELECTOR = "#ConfirmGNIB";
const SALUTATION_SELECTOR = "#Salutation";
const GIVEN_NAME_SELECTOR = '#GivenName';
const MID_NAME_SELECTOR = '#MidName';
const SUR_NAME_SELECTOR = '#SurName';
const EMAIL_SELECTOR = '#Email';
const EMAIL_CONFIRM_SELECTOR = '#EmailConfirm';
const APPLICATIONS_NUMBERS_SELECTOR = '#AppsNum'; // FOR Emergency
const APPLICATIONS_NUMBERS_VALUES = ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10 or more']; // for Emergency and Family
const CONTACT_TELEPHONE_SELECTOR = '#EmContactNum'; // for Emergency
const EMERGENCY_REASON_SELECTOR= '#EmReason';
const EMERGENCY_REASON_VALUES = ['Family bereavement', 'Serious illness of a family member'];
const APPOINTMENT_TYPE_VALUES = ['Individual', 'Family', 'Emergency'];
const USER_DECLARATION_SELECTOR = '#UsrDeclaration';
const DATE_OF_BIRTH_SELECTOR = '#DOB';
const APP_DATE_SELECTOR = '#Appdate';
const NATIONALITY_SELECTOR = '#Nationality';
const FAMAPPYN_SELECTOR= '#FamAppYN';
const FAMAPPNO_SELECTOR = '#FamAppNo';
const PPNOYN_SELECTOR = '#PPNoYN';
const PASSPORT_SELECTOR = '#PPNo';
const APPSELECTCHOICE_SELECTOR = "#AppSelectChoice";
const BTSRCH4APPS_SELECTOR = "#btSrch4Apps";
const FIND_APPOINTMENT_SLOT_SELECTOR = '#btLook4App';
const APPID_SELECTOR = "#AppID";
const SUBMIT_SELECTOR = "#Submit";
const G_RECAPTCHA_RESPONSE_SELECTOR = "#g-recaptcha-response";
const CAP_RES_SELECTOR = "#CapRes";
const AB_SELECTOR = "#AB";
const AC_SELECTOR = "#AC";
const K_SELECTOR = "#k";
const P_SELECTOR = "#p";


const api_key = "QKXQxk9XRcQa65W6Fb8YHnww6Wv7dTPFzv8J9FDA";

const parseDate = s => {
  let [d, m, y] = s.split('/')
  return chrono.parseDate([m, d, y].join('/'))
}

async function getCustomers() {
  let response = await axios.get("https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/get-all-customers-desktop?api_key="+api_key+"&format=json")
  return response.data
}

// Pull fake appointment data
async function getFakeAppointments(){
  let response = await axios.get("https://www.gnibot.com/desktops/desktop-available-fake-appointments?api_key="+api_key)
  let blocked = await getBlockedIds()
  return response.data.slots.filter(app => !blocked.includes(app.id))

}

async function getAppointments(){
  let response = await axios.get("https://gnibot.com/desktops/desktop-available-appointments?api_key="+api_key)
  let blocked = await getBlockedIds()
  if (response.data.hasOwnProperty('slots')) if ((appointments.slots[0] != 'empty')) return response.data.slots.filter(app => !blocked.includes(app.id))
  return null;

}

async function getFakeCustomers() {
  let response = await axios.get("https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/get-all-fake-customers-desktop?api_key="+api_key+"&format=json")
  if (response.data.hasOwnProperty('slots')) if ((appointments.slots[0] != 'empty')) return response.data;
  return null;
}

async function getBlockedIds(){
  let response = await axios.get("https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/get-blocked-ids-desktop?api_key="+api_key+"&fomart=json")
  return response.data;
}

async function customersForAppointments() {
  let customers = await getCustomers()
  let appointments = await getFakeAppointments()
  // let appointments = await getAppointments()
  return appointments.map(appointment => {
    let [date, time] = appointment.time.split(' - ')
    let [hours, minutes] = time.split(':').map(s => parseInt(s))
    date = chrono.parseDate(date)
    date.setHours(hours)
    date.setMinutes(minutes)
    return {
      appointment,
      customers: customers.filter(c => {
        let [start, end] = c.preference_date.split(' - ').map(d => parseDate(d))
        return date >= start && date <= end
      }).sort((a, b) => b.priority - a.priority)
    }
  })
}

async function getSession(key) {
  let session = await axios.get(`https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/get-proxy-session-inis?api_key=SXFRB6vKp29GsKuSkdf5mPpDADSVWY9vcDrqcgtJeVFV9kqTpL&key=${key}`)
  return session.data;
}

async function createBot (appointment, customer) {
  // const newProxyUrl = await proxyChain.anonymizeProxy(proxySettings);

  const browserFetcher = puppeteer.createBrowserFetcher();
  const revisionInfo = await browserFetcher.download('809590.');

  puppeteer.use(StealthPlugin());

  const browser = await puppeteer.launch({headless: false, executablePath: revisionInfo.executablePath, ignoreHTTPSErrors: true, userDataDir: './tmp', args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    // '--disable-infobars',
    // '--window-position=0,0',
    '--ignore-certifcate-errors',
    `--window-size=${viewPortSize.size.split('x')[0]},${viewPortSize.size.split('x')[1]}`,
    `--user-agent=${USER_AGENT}`, 
    // `--user-referer=${referer}`, 
    `--proxy-server=${proxySettings.proxyUrl}`
  ]});

  const page = await browser.newPage();

  await page.authenticate({
    username: 'Renan',
    password: '99uuE4',
  });
  
  await page.evaluateOnNewDocument(() => {
    const originalFunction = HTMLCanvasElement.prototype.toDataURL;
    HTMLCanvasElement.prototype.toDataURL = function (type) {
        if (type === 'image/png' && this.width === 220 && this.height === 30) {
            // this is likely a fingerprint attempt, return fake fingerprint
            return 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAANwAAAAeCAAAAABiES/iAAACeElEQVRYw+2YzUtUURjGf47OmDPh5AyFomUiEeEmyghXtWsh4dcswlYV2KYWfZh/QRBUVLhTCCJXEgmKUCIkFhJREARBkbkyKBlTRmUC82lxZ7z3TjM4whwXwz2ry3vO87znx33Pey4XFfHAg/PgPDgPzoPz4Dy4rFIKscSkAfmnsUY+iTfXFhxue4Zm4QpfaKbg8k+EsZNsGG6iNVzRMrkZeRPmjp6eCgcae5f+3wJIgtWLldG+DUnfzoail1etaVsEa1f2lUqw2hPd3T7nCrkMtlkQ24YDwP8+FZkI+gY3uq2cTcu54GIA/dJCDUAnSE4RdAESdALUxZ0hl4E5OMs49iE528E5a+cj5YFhDVI3vLA2c4K+zLXpvR37tNRDs3STg1OJqXqQSwS14wlJUD+VeHWAW86Qy8BwQ5Ek/WK/JBgqC72UTvJakmY5lAvurTRPSDrMmKRRcIvgeUo2KmmEI86Qy8DwmVu/ezQIBCSBLzwjKZhujv5cZZmUNkAq57ekRXCLYDG12pre5Qy5DAzDXbPfIOB/JqmCzNafCZd+dMA5RfZxdsBlNTAMF+FJfD2eSvSI0iGpmXe5GnbG3qyyHAO3yCZxlGV2uBLWDcJVMZKc7UrnfIBvQI+pHpxbS34ZaNkK7gYN0yvTDSCXyCZxNJTscFFe/DUH1w3QvpnzPiUPdTXfsvxZDdBGmeQU2SQd9lWQHS5m9J6Ln4/suZCwc96D25qM1formq5/3ApOX1uDkZ7P7JXkENkkK5eqQm3flRtuvitSYgCucKOf0zv01bazcG3Tyz8GKukvSjjrlB3/U5Rw42dqAo29yypKOO8figeX1/gH+zX9JqfOeUwAAAAASUVORK5CYII=';
        }
        // otherwise, just use the original function
        return originalFunction.apply(this, arguments);
    };
  });

  await page.setViewport({
    width: parseInt(viewPortSize.size.split('x')[0]),
    height: parseInt(viewPortSize.size.split('x')[1]),
    deviceScaleFactor: 1,
    hasTouch: false,
    isLandscape: false,
    isMobile: true,
  });

  await page.setUserAgent(USER_AGENT);

  await page.setJavaScriptEnabled(true);
  await page.setDefaultNavigationTimeout(0);


  await page.setRequestInterception(true);
  await page.on('request', (req) => {
    if(req.resourceType() == 'stylesheet' || req.resourceType() == 'font' || req.resourceType() == 'image'){
        req.abort();
    } else {
        req.continue();
    }
  });

  await page.evaluateOnNewDocument(() => {
    // Pass chrome check
    window.chrome = {
        runtime: {},
        // etc.
    };
  });

  await page.evaluateOnNewDocument(() => {
    // Overwrite the `plugins` property to use a custom getter.
    Object.defineProperty(navigator, 'plugins', {
        // This just needs to have `length > 0` for the current test,
        // but we could mock the plugins too if necessary.
        get: () => [1, 2, 3, 4, 5],
    });
  });

  await page.evaluateOnNewDocument(() => {
    // Overwrite the `languages` property to use a custom getter.
    Object.defineProperty(navigator, 'languages', {
        get: () => ['en-US', 'en'],
    });
  });

  await page.evaluateOnNewDocument(() => {
    //Pass notifications check
    const originalQuery = window.navigator.permissions.query;
    return window.navigator.permissions.query = (parameters) => (
        parameters.name === 'notifications' ?
            Promise.resolve({ state: Notification.permission }) :
            originalQuery(parameters)
    );
  });

  await page.evaluateOnNewDocument(() => {
    // Pass webdriver check
    Object.defineProperty(navigator, 'webdriver', {
        get: () => false,
    });
  });

  // await page.setCookie(...cookies);
  
  const canPlayTypeTs = await page.evaluate(() => {
    var audioElt = document.createElement("audio");
    return audioElt.canPlayType.toString();
    })
  console.log("canPlayTypeTs:", canPlayTypeTs);
  
  await page.goto(website_uri, { waitUntil: 'networkidle2', timeout: 0});
   
  let consent = await page.evaluate((id) => {
    let el = document.querySelector(id);
    return el ? true : false;
  }, COOKIE_CONSENT);

  if (consent) {
    await page.waitForSelector(COOKIE_CONSENT, {visible: true});
    await page.click(COOKIE_CONSENT);
  }

  await page.waitForSelector(CATEGORY_SELECTOR, {visible: true});
  await page.select(CATEGORY_SELECTOR, customer.category);

  // await page.waitForTimeout(1500);
  
  await page.waitForSelector(SUBCATEGORY_SELECTOR, {visible: true});
  await page.select(SUBCATEGORY_SELECTOR, customer.subcategory);

  // await page.waitForSelector(CONFIRMGNIB_SELECTOR, {visible: true});
  // await page.select(CONFIRMGNIB_SELECTOR, customer.confirm_gnib);

  await page.waitForSelector(USER_DECLARATION_SELECTOR, {visible: true});
  // await page.click(USER_DECLARATION_SELECTOR);

  // await page.waitForTimeout(1000);

  await page.evaluate((id) => {
    document.querySelector(id).parentElement.click();
  }, USER_DECLARATION_SELECTOR);

  // await page.waitForTimeout(1000);


  if (customer.Salutation == "" || customer.Salutation == undefined || customer.Salutation == null) 
    await page.select(SALUTATION_SELECTOR, "Mr");
  else
    await page.select(SALUTATION_SELECTOR, customer.Salutation);

  // await page.waitForTimeout(1000);

  await page.click(GIVEN_NAME_SELECTOR);
  await page.keyboard.type(customer.given_name);

  if (customer.MidName) await page.click(MID_NAME_SELECTOR);
  if (customer.MidName) await page.keyboard.type(customer.MidName);

  await page.click(SUR_NAME_SELECTOR);
  await page.keyboard.type(customer.surname);

  // await page.$eval(DATE_OF_BIRTH_SELECTOR, (el, dob) =>  {el.value = dob; }, customer.dob);

  await page.evaluate((customer) => {
    // let [day, month, year] = customer.dob.split('/')
    $('#DOB').val(customer.dob).trigger('change')
  }, customer)

  await page.select(NATIONALITY_SELECTOR, customer.nationality);
  // await page.waitForTimeout(1000);

  await page.click(EMAIL_SELECTOR);
  await page.keyboard.type(customer.temp_email);

  await page.click(EMAIL_CONFIRM_SELECTOR);
  await page.keyboard.type(customer.temp_email);
  // await page.waitForTimeout(1000);


  await page.select(FAMAPPYN_SELECTOR, customer.fam_app);
  // await page.waitForTimeout(1000);

  if (customer.fam_app == "Yes") {
    await page.waitForSelector(FAMAPPNO_SELECTOR, {visible: true});
    await page.select(FAMAPPNO_SELECTOR, customer.fam_app_num);
  }

  // await page.waitForTimeout(1000);
  await page.waitForSelector(PPNOYN_SELECTOR, {visible: true});
  await page.select(PPNOYN_SELECTOR, "Yes");

  await page.click(PASSPORT_SELECTOR);
  await page.keyboard.type(customer.passport);

  await page.click(FIND_APPOINTMENT_SLOT_SELECTOR);

  await page.waitForSelector(APPSELECTCHOICE_SELECTOR, {visible: true});
  await page.select(APPSELECTCHOICE_SELECTOR, "S");


  await page.waitForSelector(BTSRCH4APPS_SELECTOR, {visible: true});
  await page.click(BTSRCH4APPS_SELECTOR);

  
  var sessionData = await getSession(proxySettings.key);
  console.log(sessionData);

  await page.evaluate((id, app_id, ab, ac, k, p, appid, session) => {
    document.querySelector(app_id).value = appid;
    document.querySelector(ab).value = session.ab;
    document.querySelector(ac).value = session.ac;
    document.querySelector(k).value = session.k;
    document.querySelector(p).value = session.p;
    document.querySelector(id).style.opacity=1;
    document.querySelector(id).click();
  }, SUBMIT_SELECTOR, APPID_SELECTOR, AB_SELECTOR, AC_SELECTOR, K_SELECTOR, P_SELECTOR, appointment.id, sessionData);

  
  await page.waitForSelector('#ActionResult', {visible: true});
  const actionResultText =  await page.$eval('#ActionResult', el => el.innerText);
  console.log(actionResultText.replace(/(\r\n|\n|\r)/gm, " "));

  var pageUrl = page.url();
  console.log(pageUrl);

  // await page.screenshot({ fullPage: true, path: "screenshots/inis/" +customer.email+'_'+new Date().getTime()+'.png' })

  //Checking if I was redirect to suspicious page, if yes report bad captcha for 2captcha server
  if (pageUrl == "https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppErrors?OpenForm&ERR=CAP" || pageUrl == "https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppErrors?OpenForm&ERR=ERRR") {
    console.log("* By passed failed :(");
    browser.close();
    process.exit(0);
  } else {
    if (pageUrl == "https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppErrors?OpenForm&ERR=S") {
      console.log("* By passed successful :)");
      browser.close();
      process.exit(0);
    } else if (pageUrl == "https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/AppErrors?OpenForm&ERR=D") {
      console.log("* By passed successful :)");
      let deleted = await axios.get(`https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/remove-client-from-queue-desktop?email="+customer.email+"&api_key=${api_key}`);
      console.log(deleted.data);
      browser.close();
      process.exit(0);
    } else if (pageUrl.includes("https://burghquayregistrationoffice.inis.gov.ie/Website/AMSREG/AMSRegWeb.nsf/BookingConfirmed")) {
      console.log("* By passed successful and make a booking :)");
      let posted = await axios.post(`https://ht7j9z8zw3.execute-api.eu-west-1.amazonaws.com/prod/set-appointment-booked-desktop?api_key=${api_key}`, {
        email: customer.email,
        url: pageUrl
      });
      console.log(posted.data);
      browser.close();
      process.exit(0);
    }
    
  }
}

(async () => {
  try {

    // let appointments  = await getFakeAppointments();
    // console.log(appointments);

    let samples =  await customersForAppointments();

    // console.log(sample[3]);

    // for (var i in samples) {
    var {appointment, customers} = samples[3];

    var customer = customers[Math.floor(Math.random()*customers.length)];

    console.log(appointment.time, appointment.id, customers.length, customer.email);

    createBot(appointment, customer)

  } catch(e) {
    console.error(e);
    process.exit(0);
  }

})();